# ANTIC-PCKIT


### 1.) INTRODUCTION 

ANTIC-PCKIT is a solution for older and newer PC like 486, Pentium, Intel 32 bits, Intel 64 bits,... 
It features several operating systems running out of the memory/ram, like Netbsd 7.1.1. for 32MB ram machines (or less)...

**Available systems: NetBSD 7.1.1. (i386, running from GRUB to ram), NetBSD 9.1 (i386, running from GRUB to ram), Slackware 10 (to live/ramdisk), Slackware (to live/ramdisk), FreeDOS (in ram, using memdisk),... and so on.**
 
![](media/antic-pckit-logo.png)





### 2.) INSTALL

Use zcat to copy to the memstick (usb) or directly to the harddisk. The size after zcat is 500 MB.

````
zcat 1644408452-1-antic-pckit-beta-1024127-memstick-hdd-i386-bsd-slack-freedos-live-ram-v1.1.img.gz > /dev/sdX
````


### 3.) BOOT

1.) For BSD on lower mem machines

Select for instance first boot and [8] and enter: "boot netbsd-INSTALL-7.1.1.gz"

2.) x86_64 for modern machines

Select the second option Slack EFI64

2.) 486, 586,...

Select the second option Slack EFI86

(...)

### 4.) VIRTUAL-MACHINE 

It is possible to use:

````
qemu-system-i386 -hda antic-pckit.img  -m 900
````

or for text only, depending on which specific OS:

````
qemu-system-i386 -curses -hda antic-pckit.img  -m 900
````
(adapt accordingly to the serial/display.)


### 5.) LOGO 

**- Logo:**

![](media/antic-pckit-logo.png)



### 6.) Screenshots


**- Screenshots made with Qemu with graphics:**


FreeDOS:

![](media/1644410133-1-fdos1440.png)

OpenGEM:

![](media/1644409985-1-og-1.png)



**- Qemu with -curses:** 

![](media/1644410475-1-netbsd.png)




